--
-- Person provider
-- https://github.com/joke2k/faker/blob/master/faker/providers/person/__init__.py
--

CREATE OR REPLACE FUNCTION name() RETURNS TEXT
AS $$ return GD['exec'](GD['sys']._getframe()) $$ LANGUAGE plpython3u;

CREATE OR REPLACE FUNCTION last_name_male() RETURNS TEXT
AS $$ return GD['exec'](GD['sys']._getframe()) $$ LANGUAGE plpython3u;

