BEGIN;

CREATE SCHEMA IF NOT EXISTS faker;

CREATE EXTENSION IF NOT EXISTS faker SCHEMA faker CASCADE;

SELECT faker.faker();

CREATE TABLE people (
  id BIGINT,
  firstname TEXT,
  lastname TEXT,
  email TEXT,
  birth DATE,
  ssn TEXT
);

INSERT INTO people
SELECT
  faker.ean()::BIGINT,
  faker.first_name(),
  faker.last_name(),
  faker.company_email(),
  faker.faker.date_this_century()::DATE - INTERVAL '30 year',
  faker.ssn()
FROM generate_series(1,1000);

SELECT * FROM people LIMIT 2;
ROLLBACK;
